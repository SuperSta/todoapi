﻿const uri = 'api/TodoItems';

let tasks = [];
let taskAddForm = document.querySelector('.tasks__add');
taskAddForm.addEventListener("submit", addTask);

function addTask(event) {
    let taskName = event.target.querySelector('.task-name__text');
    const task = {
        isComplete: false,
        name: taskName.value.trim()
    };
    fetch(uri, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(task)
    })
        .then(response => response.json())
        .then(() => {
            getTasks();
            taskName.value = '';
        })
        .catch(error => console.error('Ошибка при добавлении задачи.', error));   
}

function getTasks() {
    fetch(uri)
        .then(response => response.json())
        .then(data => outputTasksItems(data))
        .catch(error => console.error('Невозможно получить список задач. Повторите попытку позже.', error));
}

function deleteTask(event) {
    let taskId = parseInt( event.target.getAttribute('data-task-id'),10);
    fetch(`${uri}/${taskId}`, {
        method: 'DELETE'
    })
        .then(() => getTasks())
        .catch(error => console.error('Невозможно удалить задачу.', error));
}

function updateTask(event) {
    const taskId = parseInt(event.target.getAttribute('data-task-id'),10);
    const property = event.target.getAttribute('data-property');
    const task = tasks.find(task => task.id === taskId);
    let updateValue;
 
    const item = {
        id: task.id,
        isComplete: task.isComplete,
        name: task.name
    };
    switch (property) {
        case 'name':
            updateValue = event.target.value.trim();
            break;
        case 'isComplete':
            updateValue = event.target.checked;
            
            break;
        default:
            return;
    }

    item[property] = updateValue;

    fetch(`${uri}/${task.id}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }, 
        body: JSON.stringify(item)
    })
        .then(() => getTasks())
        .catch(error => console.error('Невозможно изменить задачу.', error));

           
}

function getPlural(itemCount, single, many, many2) {

    let tenOst = itemCount % 10;
    let hundredOst = itemCount % 100;

    if (tenOst === 0 || (tenOst > 4 && tenOst < 10) || (hundredOst > 10 && hundredOst < 20)) {
        return many2;
    } else if (tenOst > 1 && tenOst < 5) {
        return many;
    } else {
        return single;
           }
}

function outputTasksCount(itemCount) {
    const name = getPlural(itemCount, 'дело', 'дела', 'дел');
    document.getElementById('counter').innerText = `${itemCount} ${name}`;
}

function outputTasksItems(data) {

    outputTasksCount(data.length);

    let taskList = document.querySelector('.task-list');
    taskList.innerHTML = '';
    data.forEach(item => {

        let task = document.createElement('div');
        task.className = 'tasks__task';

        let nameItem = document.createElement('div');
        nameItem.className = 'tasks__item task task-name';
        let nameInput = document.createElement('input');
        nameInput.className = 'task-name__text';
        nameInput.type = 'text';
        nameInput.value = item.name;
        nameInput.setAttribute('data-task-id', item.id);
        nameInput.setAttribute('data-property', 'name');
        nameInput.addEventListener('change', updateTask);

        nameItem.append(nameInput);
        task.append(nameItem);

        let isCompleteItem = document.createElement('div');
        isCompleteItem.className = 'tasks__item task task-is-complete';
        let isCompleteCheckbox = document.createElement('input');
        isCompleteCheckbox.type = 'checkbox';

        if (item.isComplete) {
            isCompleteCheckbox.setAttribute('checked', 'checked');
        }

        isCompleteCheckbox.setAttribute('data-task-id', item.id);
        isCompleteCheckbox.setAttribute('data-property', 'isComplete');
        isCompleteCheckbox.addEventListener('change', updateTask);

        isCompleteItem.append(isCompleteCheckbox);
        task.append(isCompleteItem);

        let deleteItem = document.createElement('div');
        deleteItem.className = 'tasks__item task task-action';
        let deleteButton = document.createElement('button');
        deleteButton.innerText = 'Удалить';
        deleteButton.setAttribute('data-task-id', item.id);
        deleteButton.addEventListener('click', deleteTask);
        deleteItem.append(deleteButton);
        task.append(deleteItem);
        taskList.append(task);

    });

    tasks = data;
}